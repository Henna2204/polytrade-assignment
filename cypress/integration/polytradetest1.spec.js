/// <reference types="cypress" />

describe ('Polytrade tests', () => {
    it('Check Right % APR',
        () => {
            Cypress.on('uncaught:exception', (err, runnable) => {
                // returning false here prevents Cypress from
                // failing the test
                return false
            })

            let APR;
            let fixedAPR;
            let bonusReward;
            let total;

            cy.visit("https://dev-v2.polytrade.app/")
            cy.intercept('https://rpc.ankr.com/polygon_mumbai/2f895a74c1c849a04e0f2f625b30c67f16f1f850f176c56ced21e624f4b3dc8f').as("request")
            cy.get('button[type=button]').contains('Skip').click({ force: true });
            //Getting APR %
            cy.wait("@request")
            cy.get('.col-md-8 > .d-flex > h2').wait(6000).should("not.contain", "0").invoke('text').then((string) => {
                let value = string.replace('% APR', '');
                APR = (parseFloat(value)).toFixed(2)
                cy.log(APR);
                //Getting fixed APR
                cy.get(':nth-child(5) > .h-100 > :nth-child(2) > h5').invoke('text').then((string) => {
                    value = string.replace('%', '');
                    fixedAPR = parseFloat(value)
                    cy.log(fixedAPR);
                    //Getting Bonus Reward
                    cy.get(':nth-child(6) > .h-100 > :nth-child(2) > h5').invoke('text').then((string) => {
                        value = string.replace('%', '');
                        bonusReward = parseFloat(value)
                        cy.log(bonusReward);
                        //Calculating
                        total = parseFloat(bonusReward + fixedAPR).toFixed(2);
                        expect(total).to.be.equal(APR)
                    })
                })
            })
     })

    it('Check Graph',
        () => {
            Cypress.on('uncaught:exception', (err, runnable) => {
                // returning false here prevents Cypress from
                // failing the test
                return false
            })
            cy.visit("https://dev-v2.polytrade.app/")
            cy.get('button[type=button]').contains('Skip').click({ force: true });
            cy.get(':nth-child(1) > .card > canvas').should('be.visible')
        })
})